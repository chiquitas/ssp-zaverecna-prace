import os
from datetime import datetime
import sqlite3
from sqlite3 import Error
import googleapiclient.discovery
import pytz

yt_api_key = os.getenv('YT_API')  # > in .env file

# Pomocna funkce na serazeni dat pri nalezeni videi - nalezeni nejnovejsiho

def date_sort(text):
    one_date = text['snippet']['publishedAt'].replace('T', ' ').replace('Z', '')
    _dt = datetime.strptime(one_date, '%Y-%m-%d %H:%M:%S')

    return ((_dt.month) * 30) + (_dt.day) + ((_dt.year) * 365)

# Funkce pro získání url a názvu nejnovějšího videa z YT channelu


def get_newest_youtube_video_url_and_title(channel_id):
    base_video_url = 'https://www.youtube.com/watch?v='
    api_service_name = "youtube"
    api_version = "v3"

    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey=yt_api_key)

    # ziskat datum tohoto mesice - budou se kontrolovat videa zverejnena tento
    # mesic
    now = datetime.now().replace(day=1, hour=0, minute=0, second=0)
    date_string = now.strftime("%Y-%m-%dT%H:%M:%SZ")

    # upraven request aby to hledala videa, ktera byla kdykoli tento mesic
    # vydana
    request = youtube.search().list(
        part="snippet",
        channelId=channel_id,
        maxResults=20,
        order="date",
        publishedAfter=date_string
    )
    response = request.execute()

    # pokud najde nejaka videa - porovname je a seradime podle data
    if not len(response['items']) == 0:
        response['items'].sort(reverse=True, key=date_sort)
        # print(response['items']) - pomocny print, ktery vytiskne vsechna
        # nalezena videa
        video_url = base_video_url + response['items'][0]['id']['videoId']
        video_title = response['items'][0]['snippet']['title']
        return [video_url, video_title]

    # nebyla nalezena zadna videa
    return ['NULL', 'NULL']

# Nové LP nebo OS video


async def lp_task(client):
    actual_date = datetime.now(pytz.timezone('Europe/Prague'))

    channel_id_lp = "UCdB9m-MYxtgoe1sz3ci-Zpw"
    url_lp, title_lp = get_newest_youtube_video_url_and_title(channel_id_lp)

    channel_id_os = "UCDjPwQ6VrZoptO011Y_vZqA"
    url_os, title_os = get_newest_youtube_video_url_and_title(channel_id_os)

    conn = sqlite3.connect('_db.db')
    cur = conn.cursor()

    try:
        cur.execute('SELECT * FROM LP')
        db_url_lp = cur.fetchone()

        # pridana podminka na NULL - kdybychom nenasli zadne video
        if (db_url_lp[0] != url_lp) and (url_lp != 'NULL'):
            cur.execute('UPDATE LP SET url = ?', [(url_lp)])
            conn.commit()
            print(
                '\n{:%H:%M:%S} - '.format(actual_date) +
                '\033[32mNew LP video: ' +
                title_lp +
                '\033[0m\n')

            _ch = client.get_channel(806899508901314651)
            await _ch.send('LowPeak vydal nové video! <:pogU:813077268200161322>\n' + url_lp)

        else:
            print(
                '{:%H:%M:%S} - '.format(actual_date) +
                '\033[33mNo new LP video\033[0m')

        cur.execute('SELECT * FROM LP_OS')
        db_url_os = cur.fetchone()

        # pridana podminka na NULL - kdybychom nenasli zadne video
        if (db_url_os[0] != url_os) and (url_os != 'NULL'):
            print(db_url_os[0])
            print(url_os)
            cur.execute('UPDATE LP_OS SET url = ?', [(url_os)])
            conn.commit()
            print(
                '\n{:%H:%M:%S} - '.format(actual_date) +
                '\033[32mNew OS video: ' +
                title_os +
                '\033[0m\n')

            _ch = client.get_channel(806899508901314651)
            await _ch.send('Ostravská studentská vydala nové video! <:pogU:813077268200161322>\n' + url_os)

        else:
            print(
                '{:%H:%M:%S} - '.format(actual_date) +
                '\033[33mNo new OS video\033[0m')

    except Error as _e:
        print(_e)
        _ch = client.get_channel(806899508901314651)
        await _ch.send("lp_task error: " + str(_e))

    conn.close()
