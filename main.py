######################
#     Pablo v2.9     #
######################

import urllib
import urllib.request
import os
from datetime import datetime
import asyncio
import random
import sqlite3
from sqlite3 import Error
import string
import pytz
from dotenv import load_dotenv
import requests
import discord
from discord.ext import tasks
from module_youtube import *

load_dotenv()

# modules

client = discord.Client()


# Přidání kopií slov v listu s velkým prvním písmenem
def first_upper(list):
    for _x in range(len(list)):
        list += [list[_x][0].upper() + list[_x][1:]]
    return list


##########################################################################
# Listy a konstanty
HTML_TEXT = "default"

help_list = [
    '**divocak** --> 💪🐗',
    '**amogus** --> Pošle amogus gif',
    '**pb negri** --> Vypíše počet negrů',
    '**vim** --> Pošle vim meme',
    '**pb imgur** --> Pošle random imgur obrázek',
    '**ok and @user** --> Pretty self-explanatory (tag optional)',
    '**pb thanks @user** -->  Patrick_Bateman_thanks.mp4',
    '**pb fbale / pb bale fact / pb cb fact** --> Random Christian Bale fact',
    '**pb fpsycho / pb psycho fact / pb ap fact** --> Random American Psycho fact',
    '**LP** --> LP gif',
    '**simono** --> random Simono meme',
    '**pb deadline** --> vypíše nadcházející úkoly -> pb deadline help',
    '**pb bal** --> tvůj aktuální zůstatek v Pablobance',
    '**pb sumgame** --> jednoduchá hra s možnou odměnou',
    '...a jiné eastereggy a dev commandy']

help_list_deadline = [
    '**pb deadline** --> zobrazí současné deadliny',
    '**pb deadline add name;desc;date** --> přidá deadline, datum je ve formátu dd.mm.yyyy',
    '**pb deadline del id** --> smaže deadline pomocí jeho task ID',
    '**pb deadline clear** --> vymaže celý deadline list']

negri = ["negr", "nigg", 'negř']
first_upper(negri)

divocaci = ['divočák', 'divocak', 'divočákos', 'divocakos']
first_upper(divocaci)

blacklist = ['poli', 'risc', 'RISC', 'riscovat']
first_upper(blacklist)

simona_list = [
    'ja uz to nedavam simono', 'ja uz to nedavam Simono',
    'já už to nedávám simono', 'já už to nedávám Simono', 'simono',
    'im at my limit', 'i\'m at my limit', 'v pici jsem', 'v piči jsem'
]
first_upper(simona_list)

##########################################################################


# Funkce na ziskani random imgur obrazku
def get_imgur_url():
    base_url = "https://i.imgur.com/"
    idk = string.ascii_letters + string.digits
    for _ in range(5):
        base_url += random.choice(idk)
    return base_url + '.jpg'


# Vrátí poslední adresu redirectu
def resolve(url):
    return urllib.request.urlopen(url).geturl()


# pomocna funkce na serazeni deadlinu
def sort_by_date(text):
    parts = text[4].split('.')
    return (int(parts[1]) * 30) + int(parts[0]) + (int(parts[2]) * 365)


@client.event
async def on_ready():
    actual_date = datetime.now(pytz.timezone('Europe/Prague'))
    print(
        '\033[32mConnected as {0}'.format(
            client.user) +
        ' at ' +
        '{:%H:%M:%S}\033[0m'.format(actual_date))

    # Poslat welcome message kdyz se bot zapne a pripoji na server (posle ji
    # do #bot-spam)
    channel_id = client.get_channel(945457186987974656)
    embed_var = discord.Embed(
        description='**Guess who\'s back** :sunglasses:\n(Locally)',
        color=0x39a15a)
    await channel_id.send(embed=embed_var)
    activity = discord.Game(name="Pac-Man", type=3)
    await client.change_presence(status=discord.Status.online, activity=activity)
    # time_task.start()
    call_lp_task.start()
    dvacet_dva.start()


@client.event
async def on_message(message):
    # neodpovídat na vlastní zprávy
    if message.author == client.user and message.content != 'Bruh':
        return

    # divocakos
    if any(word in message.content for word in divocaci):
        # await message.channel.send('divočákos bicákos')
        await message.reply('💪🐗')

    # amogus
    if message.content.startswith('amogus') or message.content.startswith(
            'among us') or message.content.startswith('Amogus') or message.content.startswith('Among us'):
        await message.reply(file=discord.File('Sources/Images/Amogus gifs/amogus{0}.gif'.format(random.randint(1, 5))))

    # if message.content == '!add':
    #     conn = sqlite3.connect('_db.db')
    #     cur = conn.cursor()

    #     try:
    #       cur.execute('''CREATE TABLE ukr (title varchar2)''')
    #       cur.execute('''CREATE TABLE negri (pocet int)''')
    #       cur.execute('''CREATE TABLE LP_OS (url varchar2)''')
    #       cur.execute('''CREATE TABLE LP (url varchar2)''')
    #       print('Successfully created')

    #       conn.commit()
    #     except Error as e:
    #       print(e)

    #     conn.close()

    # if message.content == '!set':
    #     conn = sqlite3.connect('_db.db')
    #     cur = conn.cursor()

    #     try:
    #       cur.execute('INSERT INTO negri VALUES (?)', [(58)])
    #       cur.execute('INSERT INTO ukr VALUES (?)', [('lmao xd')])
    #       cur.execute('INSERT INTO LP_OS VALUES (?)', [('odkaz 1 lmao')])
    #       cur.execute('INSERT INTO LP VALUES (?)', [('odkaz 2 lmao')])
    #       print('All set')

    #       conn.commit()
    #     except Error as e:
    #       print(e)

    #     conn.close()

    # if message.content == '!print':
    #     conn = sqlite3.connect('_db.db')
    #     cur = conn.cursor()

    #     try:
    #       cur.execute('SELECT * FROM ukr')
    #       title = cur.fetchone()
    #       print('Ukr: ' + str(title[0]))

    #       cur.execute('SELECT * FROM negri')
    #       pocet_ink = cur.fetchone()
    #       print('Negri: ' + str(pocet_ink[0]))

    #       cur.execute('SELECT * FROM LP_OS')
    #       odkaz1 = cur.fetchone()
    #       print('LP_OS: ' + str(odkaz1[0]))

    #       cur.execute('SELECT * FROM LP')
    #       odkaz2 = cur.fetchone()
    #       print('LP: ' + str(odkaz2[0]))

    #       conn.commit()
    #     except Error as e:
    #       print(e)

    #     conn.close()

    # if message.content == '!update':
    #     conn = sqlite3.connect('_db.db')
    #     cur = conn.cursor()

    #     try:
    #       # cur.execute('UPDATE ukr SET title = ?', [('updated title')])
    #       cur.execute('UPDATE LP_OS SET url = ?', [('updated url 1')])
    #       cur.execute('UPDATE LP SET url = ?', [('updated url 2')])
    #       print('Updated')
    #       conn.commit()
    #     except Error as e:
    #       print(e)

    #     conn.close()

    # inkrementace negrů
    if any(word in message.content for word in negri) and not (
            message.content.startswith('!') or message.content.startswith('pb')):
        conn = sqlite3.connect('_db.db')
        cur = conn.cursor()

        try:
            cur.execute('SELECT * FROM negri')
            pocet = cur.fetchone()

            cur.execute('UPDATE negri SET pocet = ?', [(pocet[0] + 1)])

            cur.execute('SELECT * FROM negri')
            pocet_ink = cur.fetchone()
            print('Negri: ' + str(pocet_ink[0]))

            conn.commit()
        except Error as _e:
            print(_e)

        conn.close()

    # vypsání počtu negrů
    if message.content.startswith('pb negri'):
        conn = sqlite3.connect('_db.db')
        cur = conn.cursor()

        try:
            cur.execute('SELECT * FROM negri')
            pocet = cur.fetchone()
            conn.commit()
            await message.reply('Slovo negr bylo vysloveno ' + str(pocet[0]) + 'x')
        except Error as _e:
            print(_e)

        conn.close()

    # gn
    if (message.content == 'gn' or message.content ==
            'Gn' or message.content == 'GN') and len(message.mentions) == 0:
        await message.reply('gn <:peepoLove:813096905634086962>')

    # vim
    if message.content == 'vim' or message.content == 'Vim' or message.content == 'VIM':
        await message.reply(file=discord.File('Sources/Images/vim.jpg'))

    # tomi paci
    if message.content.startswith(
            'ach jo') or message.content.startswith('Ach jo'):
        await message.reply(file=discord.File('Sources/Images/tomi paci.png'))

    # blacklist
    if message.content != '' and (any(word in message.content for word in blacklist[1:])
                                  or message.content.startswith(blacklist[0])
                                  or message.content.startswith(blacklist[int(len(blacklist) / 2)])):
        await message.delete()
        await message.channel.send('Bruh')
        async for msg in message.channel.history(limit=3):
            if msg.author.id == 945430515853451294 and msg.content == 'Bruh':
                await asyncio.sleep(2)
                await msg.delete()
                break

        # str(message.author) => Zinec#6251
        # message.author.name => Zinec
        # str(message.author.id) => 857016393491546132
        # message.author.discriminator => 6251
        # message.author.display_name => Žinec 🤼
        # message.author.nick => Žinec 🤼

        # await message.channel.send(message.author.avatar_url)
        # if message.author.display_name == 'Žinec 🤼':
        #   print('Je to tak')

    # random imgur from Sigma
    if message.content.startswith('pb imgur'):
        gen_url = get_imgur_url()
        while (resolve(gen_url) == 'https://i.imgur.com/removed.png'):
            gen_url = get_imgur_url()
        embed_var = discord.Embed(
            title="Random Imgur pic",
            color=0xbfbfbf,
            url=gen_url)
        embed_var.set_image(url=gen_url)
        await message.channel.send(embed=embed_var)

    # ping troll
    if message.content.startswith('pb trollping'):
        await message.delete()
        if len(message.mentions) != 0:
            i = 0
            mess = ''
            for men in message.mentions:
                mess += f'<@{str(men.id)}> '
                i += 1
            await message.channel.send(mess)
        else:
            await message.channel.send(f'<@{str(message.author.id)}>')

    # vypnutí bota
    if message.content == 'pb shutdown' or message.content == 'sudo rm -rf pablo' or message.content == 'Sudo rm -rf pablo' or message.content == 'sudo rm -rf Pablo' or message.content == 'Sudo rm -rf Pablo':
        await message.channel.send('>:(', file=discord.File('Sources/Images/shutdown.gif'))
        os._exit(0)

    # ok and
    if message.content.startswith(
            'ok and') or message.content.startswith('Ok and'):
        await message.delete()
        i = 0
        mess = ''
        for men in message.mentions:
            mess += f'<@{str(men.id)}> '
            i += 1
        await message.channel.send(mess, file=discord.File('Sources/Images/okand.gif'))

    # thanks
    if message.content.startswith('pb thanks'):
        if len(message.mentions) != 0:
            await message.delete()
            i = 0
            mess = ''
            for men in message.mentions:
                mess += f'<@{str(men.id)}> '
                i += 1
            await message.channel.send(mess, file=discord.File('Sources/Images/Patrick Bateman/thanks.mp4'))
        else:
            await message.channel.send('Musíš někoho označit, hlupáku')

    # christian bale facts
    if message.content == 'pb fbale' or message.content == 'pb bale fact' or message.content == 'pb cb fact':
        with open('Sources/CB_facts.txt') as file:
            lines = file.readlines()

        gen = random.randint(0, len(lines) - 1)
        embed_var = discord.Embed(
            title="Random Christian Bale fact",
            description=lines[gen],
            color=0xbfbfbf)
        path = 'Sources/Images/Patrick Bateman/bale{0}.jpg'.format(
            random.randint(1, 11))
        file = discord.File(path, filename='bale.jpg')
        # embed_var.set_thumbnail(url='attachment://bale.jpg')
        embed_var.set_image(url='attachment://bale.jpg')
        await message.channel.send(file=file, embed=embed_var)
        # await message.channel.send(embed=embed_var)

    # american psycho facts
    if message.content == 'pb fpsycho' or message.content == 'pb psycho fact' or message.content == 'pb ap fact':
        with open('Sources/AP_facts.txt') as file:
            lines = file.readlines()

        rnd = random.randint(0, len(lines) - 1)
        split_lines = lines[rnd].split('|')
        embed_var = discord.Embed(
            title="Random American Psycho fact",
            color=0xbfbfbf)
        embed_var.add_field(
            name=split_lines[0],
            value=split_lines[1],
            inline=False)
        path = 'Sources/Images/Patrick Bateman/bale{0}.jpg'.format(
            random.randint(1, 11))
        file = discord.File(path, filename='bale.jpg')
        # embed_var.set_thumbnail(url='attachment://bale.jpg')
        embed_var.set_image(url='attachment://bale.jpg')
        await message.channel.send(file=file, embed=embed_var)
        # await message.channel.send(embed=embed_var)

    # help
    if message.content.startswith('pb help'):
        help_str = ''
        for i in range(len(help_list) - 1):
            help_str += '•  ' + help_list[i] + '\n'
        help_str += help_list[len(help_list) - 1]

        embed_var = discord.Embed(
            title="List of available commands:",
            color=0xffaf41,
            description=help_str)
        file = discord.File('Sources/Images/jarvis.gif', filename='jarvis.gif')
        embed_var.set_image(url='attachment://jarvis.gif')
        await message.channel.send(file=file, embed=embed_var)

    # pablo
    if message.content == 'pablo' or message.content == 'Pablo':
        embed_var = discord.Embed(
            title="Pablo.",
            description='Pablo.',
            color=0xbfbfbf)
        embed_var.add_field(name="Pablo.", value='Pablo.', inline=False)
        file = discord.File('Sources/Images/pablo.jpg', filename='pablo.jpg')
        embed_var.set_image(url='attachment://pablo.jpg')
        await message.channel.send(file=file, embed=embed_var)

    # LP
    if message.content == 'LP':
        await message.delete()
        await message.channel.send('https://tenor.com/view/low-peak-gotem-lowpeak-gotem-gif-24447920')

    # jindruv pojebany kun
    # if any(word in message.content for word in list('kun'):
    # await
    # message.channel.send(file=discord.File('Sources/Images/Kun/kun{0}.jpg'.format(random.randint(1,8))))

    # main deadline command
    try:
        if message.content == 'pb deadline':
            embed_var = discord.Embed(
                title='Incoming deadlines',
                description="For a list of commands type '!deadline help'",
                color=0x39a15a)
            conn = sqlite3.connect('_db.db')
            cur = conn.cursor()
            try:
                cur.execute('SELECT * FROM Task')
                rows = cur.fetchall()
                rows.sort(key=sort_by_date)
                for row in rows:
                    # print(row)
                    text = "**" + row[4] + "** | " + row[3] + "\n*Added by user:* <@" + str(
                        row[1]) + "> *, task ID = " + str(row[0]) + "*"
                    embed_var.add_field(name=row[2], value=text, inline=False)

                if len(rows) == 0:
                    await message.channel.send('**[Deadline]** There are no deadlines, let\'s gooo')
                else:
                    await message.channel.send(embed=embed_var)
                conn.commit()

            except Error as _e:
                print(_e)

            conn.close()

    except IndexError as _e:
        await message.channel.send("**[Python Core]** Error occured: " + str(_e))
        await message.channel.send("**[Deadline]** Showing unformatted and unsorted data due to unknown error:")
        conn = sqlite3.connect('_db.db')
        cur = conn.cursor()
        rows = cur.fetchall()
        for row in rows:
            await message.channel.send(' - ' + row[2] + ', ' + row[3] + ', ' + row[4] + ', task id: ' + str(row[0]) + ', user id: ' + str(row[1]))

    # add into deadlist
    if message.content.startswith('pb deadline add'):
        # parse arguments
        parts = message.content[14:].split(';')

        if not len(parts) == 3:
            await message.channel.send('**[Deadline]** Invalid syntax of \'add\'. Please follow syntax: "**pb deadline add name;description;date**" - date format is dd.mm.yyyy')
            return

        if not len(parts[2].split('.')) == 3:
            await message.channel.send('**[Deadline]** Invalid syntax of \'date\' in \'add\'. Please follow syntax: "**pb deadline add name;description;date**" - date format is dd.mm.yyyy')
            return

        name = parts[0]
        desc = parts[1]
        date = parts[2]
        conn = sqlite3.connect('_db.db')
        cur = conn.cursor()

        # try to add into database
        try:
            command = ("INSERT INTO Task (user_id, name, desc, datetime) VALUES (" +
                       str(message.author.id) +
                       ", '" +
                       str(name) +
                       "', '" +
                       str(desc) +
                       "', '" +
                       str(date) +
                       "')")
            cur.execute(command)
            conn.commit()
            command = "SELECT task_id FROM Task WHERE desc LIKE '" + \
                str(desc) + "' AND name LIKE '" + str(name) + "'"
            cur.execute(command)
            row = cur.fetchone()
            print('Set.')
            await message.channel.send('**[Deadline]** Added new deadline with task ID: ' + str(row[0]) + '.')

            conn.commit()
        except Error as _e:
            print(_e)

        conn.close()

    # delete certain deadline
    if message.content.startswith('pb deadline del'):
        parts = message.content.split(' ')
        # task_id = parts[2]
        conn = sqlite3.connect('_db.db')
        cur = conn.cursor()
        try:
            command = "DELETE FROM Task WHERE task_id = " + parts[2]
            cur.execute(command)
            print('Deleted.')
            await message.channel.send('**[Deadline]** Deleted deadline with ID: ' + parts[2] + '.')
            conn.commit()
        except sqlite3.Error as _e:
            print(_e)
            await message.channel.send('**[Deadline]** Failed to delete deadline.')

        conn.close()

    # clear whole deadline list
    if message.content == 'pb deadline clear':
        conn = sqlite3.connect('_db.db')
        cur = conn.cursor()

        try:
            command = "DELETE FROM Task"
            cur.execute(command)
            print('Deleted all.')
            await message.channel.send('**[Deadline]** Deleted all deadlines.')

            conn.commit()
        except Error as _e:
            print(_e)

        conn.close()

    # show deadline help message
    if message.content == 'pb deadline help':
        text = ""
        for hlp in help_list_deadline:
            text += hlp
            text += '\n'
        embed_var = discord.Embed(
            title='List of commands',
            description=text,
            color=0x39a15a)
        await message.channel.send(embed=embed_var)

    # Simona
    if any(word in message.content for word in simona_list):
        random.seed(datetime.now())
        await message.channel.send(file=discord.File('Sources/Images/Simona/simona' + str(random.randint(1, 5)) + '.jpg'))

    # Balance
    if message.content == "pb bal":
        conn = sqlite3.connect('_db.db')
        cur = conn.cursor()
        command = "SELECT amount FROM Balance WHERE user_id = " + \
            str(message.author.id)
        cur.execute(command)
        amount = cur.fetchone()
        conn.close()
        await message.reply("Tvůj zůstatek: " + str(amount[0]))

    # Sum game
    if message.content == "pb sumgame":
        orig_msg = await message.reply("Připravit")
        await asyncio.sleep(1)
        await orig_msg.edit(content="Pozor")
        _a = random.randint(1, 100)
        _b = random.randint(1, 100)
        await asyncio.sleep(1)
        await orig_msg.edit(content="Kolik je " + str(_a) + " + " + str(_b) + "?")

        def check(_m):
            return _m.author == message.author and _m.channel == message.channel

        try:
            msg = await client.wait_for("message", check=check, timeout=5)
            try:
                if (int(msg.content) == _a + _b):
                    await msg.reply(content="Přesně tak, získáváš +50 na svůj účet <:pogU:987084984768659457>")
                    conn = sqlite3.connect('_db.db')
                    cur = conn.cursor()
                    cur.execute(
                        'UPDATE Balance SET amount = amount + 50 WHERE user_id = ?', [(str(message.author.id))])
                    conn.commit()
                    conn.close()
                else:
                    await msg.reply(content="Dumbass")
            except ValueError:
                await msg.reply(content="Dumbass")
        except asyncio.TimeoutError:
            await message.channel.send(f"<@{str(message.author.id)}> <:Sleep:827701221720326154>")

    if message.content == "pb penis":
        _l = random.randint(0, 15)
        _pp = "B"
        for i in range(_l):
            _pp += "="
        _pp += "D"
        await message.reply(content=_pp)

# Články o Ukrajině


@tasks.loop(seconds=30)
async def ukrajina():
    global HTML_TEXT
    actual_date = datetime.now(pytz.timezone('Europe/Prague'))
    page = requests.get('https://denikn.cz/api/sitemap/news')
    start_text = page.text.find("<news:title>")
    start_time = page.text.find("<news:publication_date>")
    end_text = page.text.find("</news:title>")
    end_time = page.text.find("</news:publication_date>")

    if HTML_TEXT != page.text[start_text + 12:end_text]:
        HTML_TEXT = page.text[start_text + 12:end_text]
        html_time = page.text[start_time + 23:end_time].split('T')
        date = html_time[0].split(
            '-')[2] + "." + html_time[0].split('-')[1] + "." + html_time[0].split('-')[0]
        title = "Deník N - " + html_time[1].split('+')[0] + " dne " + date

        conn = sqlite3.connect('_db.db')
        cur = conn.cursor()

        try:
            cur.execute('SELECT * FROM ukr')
            db_title = cur.fetchone()

            if title != db_title[0]:
                cur.execute('UPDATE ukr SET title = ?', [(title)])
                cur.execute('SELECT * FROM ukr')
                db_title_updated = cur.fetchone()
                print('\n{:%H:%M:%S} - '.format(actual_date) +
                      '\033[32mNew article: ' +
                      page.text[start_text +
                                12:start_text +
                                50] +
                      '...\033[0m\n')
                conn.commit()

                _ch = client.get_channel(946559291832295484)
                embed_var = discord.Embed(
                    description=page.text[start_text + 12:end_text], color=0xbfbfbf)
                embed_var.set_author(name=db_title_updated[0])
                await _ch.send(embed=embed_var)

        except Error as _e:
            print(_e)

        conn.close()
    else:
        print(
            '{:%H:%M:%S} - '.format(actual_date) +
            '\033[33mNo new article\033[0m')


# Nové LP nebo OS video
@tasks.loop(minutes=30)
async def call_lp_task():
    await lp_task(client)


# Zpráva v onen magický čas
@tasks.loop(minutes=1)
async def dvacet_dva():
    now = datetime.now(pytz.timezone('Europe/Prague'))
    if now.hour == 22 and now.minute == 22:
        channel_id = client.get_channel(806899508901314651)
        msg = 'Je 22:22, něco si přejte! <:pogU:813077268200161322>'
        embed_var = discord.Embed(description=msg, color=0xbfbfbf)
        await channel_id.send(embed=embed_var)


# Vymazání konzole
os.system('clear')

# |\
client.run(os.getenv('TOKEN'))  # | > in .env file
# |/
