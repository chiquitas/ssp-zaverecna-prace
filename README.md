# **Pablo - OST0052 & CEC0144**

A Discord bot for a private server

![Pablo obviously](https://gitlab.com/uploads/-/system/project/avatar/36964807/tf4vq28rer261.jpg)

- Functions:
  - Word counters
  - Frequent information about Russia-Ukraine war
  - Inform about new videos of a certain YouTube channel
  - Basic deadlines management
  - and many more...

- Known issues:
  - None, it's perfect :sunglasses:

- Work in progress:
  - Seperation into multiple files
  - Better Pylint result - improve basic syntax and rules
